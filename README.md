Add IE className to document
============================

Quick drop-in plugin adaptation of [detectie](http://npmjs.com/detectie).    
Adds the following classes to the HTML classlist:

  + 'js'
  + When IE is detected:
    + 'ie'
    + 'ie' with version number

```js
var isIE = require('add-ie-classname')();
console.log(isIE); // True if IE browser
```

## Running Tests client side

make sure you have webpack installed globally

```shell
npm install webpack -g
```

compile test.js to a bundle with webpack

```shell
npm test
```

open test.html to view the tests in the browser

## License

[MIT](http://isekivacenz.mit-license.org/)
