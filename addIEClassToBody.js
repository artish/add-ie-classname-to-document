/**
 * Very simple wrapper around the detectIE package
 * @return {string} The IE version
 */
var detectIE = require('detectIE');
module.exports = function() {

  var ieVersion = detectIE();
  var htmlEl = document.getElementsByTagName('html')[0];
  var cachedClassName = 'js'; // Add js to the className at all times

  function addToHTMLClass(className) {
    htmlEl.className += ' ' + className;
  }

  if (ieVersion) {
    cachedClassName = cachedClassName + ' ie ie' + ieVersion;
  }

  addToHTMLClass(cachedClassName)

  return ieVersion;
};
