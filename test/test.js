var chai = require('chai');
var detectie = require('../addIEClassToBody');
var assert = chai.assert;
var expect = chai.expect;

describe('detectie()', function() {

  var isIE = detectie();

  it('should be false on non IE browsers', function() {
    assert.equal(isIE, false);
  })

  console.log(isIE);

  it('should return version number on IE', function() {
    assert.isNumber(isIE, true, 'should be true on IE');
  })

  it('Should contain ie in the classlist on IE browsers', function() {
    assert.include(document.getElementsByTagName('html')[0].classList.toString().split(' '), 'ie', 'should be true on IE');
  })

})
